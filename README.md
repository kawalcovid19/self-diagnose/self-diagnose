# Self Diagnose

Documentation for Self Diagnose Team

## Goal

1. Semakin banyak orang yang ingin memeriksakan diri/aware tentang bahaya Covid-19 (better false positive daripada false negative).
2. Meningkatkan awareness
3. Mendorong demand untuk screening jika ada gejala
4. Mendorong social distancing

## Reference

[Coronavirus Signs & Symptoms Test](https://www.coronavirus-signs-and-symptoms.com/en/)

## Resource

[Daftar Pertanyaan untuk Survey](https://docs.google.com/document/d/1tlhn9x2U9hXViSP0Lioi-LJNEqD0hdg8YMXqcTorOfs/edit) 

[Draft Survey](https://feedloop.io/q/covid19selfassesment)

[Board untuk Tracking](https://gitlab.com/kawalcovid19/self-diagnose/-/boards)

## Sub-teams

 1. Team Survey: Konten survey & perbaikan form
 2. Team Konten Result: Pembuatan web di kawalcovid19.id
 3. Team App: Development wrapper untuk survey & use case lainnya

## Roles

-   PMs/Analyst/people: to research things
-   UIUX: to design beautiful interfaces and user experiences
-   Front-end: to create the webapp
-   Back-end/data: to ingest the data from [feedloop.io](http://feedloop.io/)
-   Content: to help bridge what we need from [#team-website](https://kawalcovid19.slack.com/archives/CV4EFSEBS) , [#team-content](https://kawalcovid19.slack.com/archives/CV8HGFTDM) , and team medic.

