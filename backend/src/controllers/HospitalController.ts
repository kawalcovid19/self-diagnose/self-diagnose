import { Response } from "express";
import { Get, Header, HttpCode, JsonController, QueryParam, Res, Param } from "routing-controllers";
import { OpenAPI, ResponseSchema } from "routing-controllers-openapi";
import { isNullOrUndefined } from "util";

import { Hospital } from "../entities/Hospital";
import { HospitalService } from "../services/HospitalService";
import { logger } from "../utils/Logger";

const responseMessage = {
    RESULT_NOT_FOUND: "RESULT_NOT_FOUND",
    RESULT_NOT_FOUND_ON_PROVINCE: "RESULT_NOT_FOUND_ON_PROVINCE",
    RESULT_NOT_FOUND_ON_CITY: "RESULT_NOT_FOUND_ON_CITY",
};

@JsonController("/hospitals")
export class HospitalController {
    constructor(private hospitalService: HospitalService) {}

    @HttpCode(200)
    @Header("Cache-Control", "public, s-maxage=60, stale-while-revalidate")
    @Get("")
    @ResponseSchema(Hospital)
    @OpenAPI({
        summary: "List reference hospital",
        description: "This route provide all reference hospitals",
        responses: {
            "200": {
                description: "return list of hospital",
            },
        },
    })
    public async getAll(@QueryParam("query") query: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitals(query);

        if (isNullOrUndefined(hospitals) || !hospitals.length) {
            logger.info(responseMessage.RESULT_NOT_FOUND);
            return res.status(200).send([]);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Header("Cache-Control", "public, s-maxage=60, stale-while-revalidate")
    @Get("/province")
    @ResponseSchema(Hospital)
    @OpenAPI({
        summary: "List reference hospital by province",
        description: "This route provide hospital by province query",
        responses: {
            "200": {
                description: "return list of hospital at province ${name}",
            },
        },
    })
    public async getProvince(@QueryParam("name") name: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalsByProvince(name);

        if (isNullOrUndefined(hospitals) || !hospitals.length) {
            logger.info(responseMessage.RESULT_NOT_FOUND_ON_PROVINCE + ":" + name.toUpperCase());
            return res.status(200).send([]);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Header("Cache-Control", "public, s-maxage=60, stale-while-revalidate")
    @Get("/city")
    @OpenAPI({
        summary: "List reference hospital by city",
        description: "This route provide hospital by city query",
        responses: {
            "200": {
                description: "return list of hospital at city ${name}",
            },
        },
    })
    public async getCity(@QueryParam("name") name: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalsByCity(name);

        if (isNullOrUndefined(hospitals) || !hospitals.length) {
            logger.info(responseMessage.RESULT_NOT_FOUND_ON_CITY + ":" + name.toUpperCase());
            return res.status(200).send([]);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Header("Cache-Control", "public, s-maxage=60, stale-while-revalidate")
    @Get("/hospital")
    @OpenAPI({
        summary: "List reference hospital by name",
        description: "This route provide hospital by name query",
        statusCode: "200",
    })
    public async getName(@QueryParam("name") name: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalsByName(name);

        if (isNullOrUndefined(hospitals) || !hospitals.length) {
            logger.info("No hospital found for name: " + name);
            return res.status(200).send([]);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Header("Cache-Control", "public, s-maxage=60, stale-while-revalidate")
    @Get("/get/:id")
    @OpenAPI({
        summary: "List reference hospital by id",
        description: "This route provide hospital by hospital ID",
        statusCode: "200",
    })
    public async getHospitalID(@Param("id") id: string, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalsByID(id);

        if (isNullOrUndefined(hospitals) || !hospitals.length) {
            logger.info("No hospital found for id: " + id);
            return res.status(200).send([]);
        }
        return hospitals;
    }

    @HttpCode(200)
    @Header("Cache-Control", "public, s-maxage=60, stale-while-revalidate")
    @Get("/nearby")
    @OpenAPI({
        summary: "List reference hospital nearby given lat and long",
        description: "This route provide hospital nearby given lat and long",
        statusCode: "200",
    })
    public async getNearby(@QueryParam("lat") lat: number, @QueryParam("long") long: number, @Res() res: Response) {
        const hospitals = await this.hospitalService.getHospitalNearby(lat, long);

        if (isNullOrUndefined(hospitals) || !hospitals.length) {
            logger.info(`No hospital found for coords: ${lat},${long}`);
            return res.status(200).send([]);
        }
        return hospitals;
    }
}
