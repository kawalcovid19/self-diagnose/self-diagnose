//I only need a banana, i don't want to bring a whole zoo with me
//https://github.com/kvz/locutus/blob/master/src/php/math/deg2rad.js
export const deg2rad = (angle: number): number => {
    //  discuss at: https://locutus.io/php/deg2rad/
    // original by: Enrique Gonzalez
    // improved by: Thomas Grainger (https://graingert.co.uk)
    //   example 1: deg2rad(45)
    //   returns 1: 0.7853981633974483

    return angle * 0.017453292519943295; // (angle / 180) * Math.PI;
};

// https://github.com/kvz/locutus/blob/master/src/php/math/rad2deg.js
export const rad2deg = (angle: number): number => {
    //  discuss at: https://locutus.io/php/rad2deg/
    // original by: Enrique Gonzalez
    // improved by: Brett Zamir (https://brett-zamir.me)
    //   example 1: rad2deg(3.141592653589793)
    //   returns 1: 180

    return angle * 57.29577951308232; // angle / Math.PI * 180
};
