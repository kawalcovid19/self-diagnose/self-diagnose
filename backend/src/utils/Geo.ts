import { deg2rad, rad2deg } from './Math';

//shamelessly copy-pasted from https://ourcodeworld.com/articles/read/1019/how-to-find-nearest-locations-from-a-collection-of-coordinates-latitude-and-longitude-with-php-mysql
export const getDistanceBetweenPoints = (lat: number, long: number, destLat: number, destLong: number) => {
    const theta = long - destLong;
    let distance = Math.sin(deg2rad(lat)) * Math.sin(deg2rad(destLat)) + Math.cos(deg2rad(lat)) * Math.cos(deg2rad(destLat)) * Math.cos(deg2rad(theta));

    distance = Math.acos(distance);
    distance = rad2deg(distance);
    distance = distance * 60 * 1.1515;

    //convert to KM
    distance = distance * 1.609344;

    return (Math.round(distance));
}
