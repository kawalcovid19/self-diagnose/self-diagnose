import { expressToOpenAPIPath } from "routing-controllers-openapi";
import { Connection } from "typeorm";

import { Hospital } from "../../../src/entities/Hospital";
import { HospitalRepository } from "../../../src/repositories/HospitalRepository";
import { HospitalService } from "../../../src/services/HospitalService";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { HospitalSeed } from "../../utils/seeds/HospitalTestSeed";

describe("HospitalService", () => {
    let db: Connection;
    let hospitalRepository: HospitalRepository;
    let hospitalService: HospitalService;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        hospitalRepository = db.getCustomRepository(HospitalRepository);
        hospitalService = new HospitalService(hospitalRepository);
        await hospitalRepository.save(HospitalSeed);
    });

    afterAll(() => db.close());

    const request = {
        id: 1,
        province: "Aceh",
        city: "Lhokseumawe",
        name: "RSU Dr. Zainoel Abidin Banda Aceh",
        lat: -0.943174,
        long: 100.366944,
    };

    it("Get all hospital", async () => {
        const hospitals = await hospitalService.getHospitals();
        expect(hospitals).not.toBe(null);
        expect(hospitals[0]).toBeInstanceOf(Object);
    });

    it("Get hospital by province", async () => {
        const hospitals = await hospitalService.getHospitalsByProvince(request.province);
        expect(hospitals[0].province).toBe(request.province);
    });

    it("Get hospital by city", async () => {
        const hospitals = await hospitalService.getHospitalsByCity(request.city);
        expect(hospitals[0].city).toBe(request.city);
    });

    it("Get hospital by name", async () => {
        const hospitals = await hospitalService.getHospitalsByName(request.name);
        expect(hospitals[0].name).toBe(request.name);
    });

    it("Get hospital by name", async () => {
        const hospitals = await hospitalService.getHospitalNearby(request.lat, request.long);
        expect(hospitals).not.toBe(null);
        expect(hospitals[0]).toBeInstanceOf(Object);
    });

    it("Get hospital by name", async () => {
        const hospitals = await hospitalService.getHospitalsByName(request.name);
        expect(hospitals[0].name).toBe(request.name);
    });

    it("Get hospital by name", async () => {
        const hospitals = await hospitalService.getHospitalNearby(request.lat, request.long);
        expect(hospitals).not.toBe(null);
        expect(hospitals[0]).toBeInstanceOf(Object);
    });

    it("Get hospital by ID", async () => {
        const hospitals = await hospitalService.getHospitalsByID(request.id.toString());
        expect(hospitals).not.toBe(null);
    });
});
