import { Connection } from "typeorm";
import { createMemoryDatabase } from "../../utils/CreateMemoryDatabase";
import { SurveyService } from "../../../src/services/SurveyService";
import { SurveyRepository } from "../../../src/repositories/SurveyRepository";

describe("SurveyService", () => {
    let db: Connection;
    let surveyRepository: SurveyRepository;
    let surveyService: SurveyService;

    beforeAll(async () => {
        db = await createMemoryDatabase();
        surveyRepository = db.getCustomRepository(SurveyRepository);
        surveyService = new SurveyService(surveyRepository);
    });

    afterAll(() => db.close());

    const surveyRequest = {
        "age": 17,
        "browserInfo": "MozillaFirefox/ChromeOSX10",
        "location": "Serpong",
        "status": "warning",
        "questions": [
            {
                "questionId": 2,
                "question": "Apakah anda batuk?",
                "answer": true
            },
            {
                "questionId": 1,
                "question": "Apakah anda sesak nafas?",
                "answer": true
            }
        ]
    };

    it("Create a survey and return the created information", async () => {
        const newSurvey = await surveyService.createSurvey(surveyRequest as any);
        expect(newSurvey.age).toBe(surveyRequest.age);
        expect(newSurvey.browserInfo).toBe(surveyRequest.browserInfo);
        expect(newSurvey.location).toBe(surveyRequest.location);
        expect(newSurvey.status).toBe(surveyRequest.status);
        expect(newSurvey.questions).toBe(surveyRequest.questions);
    });
});
