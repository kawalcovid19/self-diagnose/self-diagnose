import * as React from 'react'
import styled from '@emotion/styled'
import { themeGet } from '@styled-system/theme-get'

import { themeProps, Box } from 'components/design-system'
import Link from 'next/link'
import { logEventClick } from 'utils/analytics'

interface NavLinkProps {
  title: string
  href: string
  as?: string
  isActive?: boolean
  icon?: React.ReactNode
}

export const MainNavCenter = styled(Box)`
  display: block;
  padding-left: 16px;

  ${themeProps.mediaQueries.md} {
    padding-top: 4px;
    padding-left: 16px;
  }
`

export const MainNavCenterLinks = styled(Box)`
  display: none;

  ${themeProps.mediaQueries.sm} {
    display: block;
  }
`

export const MainNavRight = styled(Box)`
  padding-top: 2px;

  ${themeProps.mediaQueries.md} {
    padding-top: 10px;
  }
`

export const MainNavLinkBase = styled('a')<Pick<NavLinkProps, 'isActive'>>`
  position: relative;
  display: inline-block;
  text-decoration: none;

  &::before {
    display: ${props => (props.isActive ? `block` : 'none')};
    content: '';
    position: absolute;
    height: 2px;
    bottom: 0;
    background-color: ${themeGet('colors.brandred', themeProps.colors.brandred)};
  }

  ${themeProps.mediaQueries.sm} {
    padding: 8px;
    font-size: 13px;

    &::before {
      width: calc(100% - 17px);
      left: 7px;
    }
  }
  ${themeProps.mediaQueries.md} {
    padding: 10px;
    font-size: 15px;
    &::before {
      width: calc(100% - 20px);
      left: 10px;
    }
  }
  ${themeProps.mediaQueries.lg} {
    padding: 15px;
    font-size: 16px;
    &::before {
      width: calc(100% - 30px);
      left: 15px;
    }
  }
`

const MainNavInner: React.FC = ({ children }) => {
  return (
    <Box
      display="flex"
      flexDirection="row"
      alignItems="center"
      gridColumn="3/4"
      overflowX="auto"
      overflowY="hidden"
      height={[60, null, null, 96, null]}
    >
      {children}
    </Box>
  )
}

const MainNavLink: React.FC<NavLinkProps> = ({ title, href, as, isActive }) => {
  return (
    <Link href={href} as={as} passHref>
      <MainNavLinkBase isActive={isActive} onClick={() => logEventClick(title)}>
        {title}
      </MainNavLinkBase>
    </Link>
  )
}

export { MainNavInner, MainNavLink }
