import * as React from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import styled from '@emotion/styled'
import VisuallyHidden from '@reach/visually-hidden'
import { themeProps, Box, UnstyledButton, Text } from 'components/design-system'
import { logEventClick } from 'utils/analytics'
import useDarkMode from 'utils/useDarkMode'
import Logo from './Logo'
import {
  NavGrid,
  MainNavInner,
  MainNavCenter,
  MainNavCenterLinks,
  MainNavLink,
  MainNavRight,
  MobileNav,
  MobileNavLink
} from './library'
import {
  ChevronIcon,
  HomeIcon,
  OptionIcon,
  HealthMeterIcon,
  MedicalIcon,
  SearchIcon
} from 'components/icons'
import ColorToggle from './library/ColorToggle'
import OptionModal from './library/OptionModal'

interface NavigationProps {
  pageTitle?: string
  hideBottomNav?: boolean
}

const Root = Box.withComponent('header')

const LogoLinkRoot = Box.withComponent('a')

const LogoLink = styled(LogoLinkRoot)`
  display: block;
  width: 56px;
  height: 48px;
  overflow: hidden;

  ${themeProps.mediaQueries.md} {
    width: 80px;
    height: 64px;
  }

  &:hover,
  &:focus,
  &:active,
  &:visited {
    text-decoration: none;
  }
`

const LogoWrapper = styled(Box)`
  > svg {
    ${themeProps.mediaQueries.md} {
      width: 80px !important;
      height: 64px !important;
    }
  }
`

const PageTitle = styled(Box)`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${themeProps.mediaQueries.sm} {
    display: none;
  }
`

const SearchButton = styled(UnstyledButton)`
  display: inline-flex;
  align-items: center;
  justify-content: center;
  outline: none;
  display: block;

  ${themeProps.mediaQueries.md} {
    display: none;
  }
`

const OptionButton = styled(UnstyledButton)`
  display: flex;
  flex-direction: column;
  flex: 1;
  justify-content: center;
  align-items: center;
  font-size: 10px;
  font-weight: 600;
  padding-bottom: 8px;
`

const OptionButtonIcon = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 24px;
  height: 24px;
  margin-bottom: 4px;
`

const Navigation: React.FC<NavigationProps> = ({ pageTitle, hideBottomNav }) => {
  const [isDarkMode, toggleDarkMode] = useDarkMode()
  const [isOptionModalOpen, setIsOptionModalOpen] = React.useState(false)
  const router = useRouter()

  const toggleOptionModal = () => {
    setIsOptionModalOpen(!isOptionModalOpen)
  }

  const handleSearch = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth'
    })
  }

  const colorToggleProps = {
    isDarkMode,
    toggleDarkMode
  }

  const { pathname } = router
  const mobileButtonColor = (path: string) => {
    if (pathname === path) {
      return '#3389fe'
    }
  }

  return (
    <Root position="fixed" width="100%">
      <NavGrid backgroundColor="accents01">
        <MainNavInner>
          <Link href="/" passHref>
            <LogoLink onClick={() => logEventClick('Beranda')}>
              <VisuallyHidden>Kawal COVID-19</VisuallyHidden>
              <LogoWrapper display="flex" alignItems="flex-end">
                <Logo aria-hidden />
              </LogoWrapper>
            </LogoLink>
          </Link>
          <MainNavCenter flex="1 1 auto">
            {pageTitle && (
              <PageTitle>
                <ChevronIcon />
                <Text ml="sm">{pageTitle}</Text>
              </PageTitle>
            )}
            <MainNavCenterLinks>
              <MainNavLink href="/" isActive={pathname === '/'} title="Beranda" />
              <MainNavLink
                href="/periksa-mandiri"
                isActive={pathname === '/periksa-mandiri'}
                title="Periksa Mandiri"
              />
              <MainNavLink
                href="/rs-rujukan"
                isActive={pathname === '/rs-rujukan' || pathname === '/rs-rujukan/detail'}
                title="RS Rujukan"
              />
              {/* <MainNavLink href="/panduan" isActive={pathname === '/panduan'} title="Panduan" /> */}
            </MainNavCenterLinks>
          </MainNavCenter>
          <MainNavRight display="flex" alignItems="center">
            <ColorToggle {...colorToggleProps} />
            {pathname === '/rs-rujukan' && (
              <SearchButton type="button" ml="md" onClick={handleSearch}>
                <SearchIcon />
              </SearchButton>
            )}
          </MainNavRight>
        </MainNavInner>
      </NavGrid>
      {!hideBottomNav && (
        <NavGrid backgroundColor="accents01">
          <MobileNav backgroundColor="accents01">
            <MobileNavLink
              href="/"
              isActive={pathname === '/'}
              title="Beranda"
              icon={<HomeIcon width={24} fill={mobileButtonColor('/')} />}
            />
            <MobileNavLink
              href="/rs-rujukan"
              isActive={pathname === '/rs-rujukan'}
              title="RS Rujukan"
              icon={<MedicalIcon width={24} fill={mobileButtonColor('/rs-rujukan')} />}
            />
            <MobileNavLink
              href="/periksa-mandiri"
              isActive={pathname === '/periksa-mandiri'}
              title="Cek Mandiri"
              icon={<HealthMeterIcon width={24} fill={mobileButtonColor('/periksa-mandiri')} />}
            />
            {/* <MobileNavLink
            href="/panduan"
            isActive={pathname === '/panduan'}
            title="Karantina"
            icon={<QuarantineIcon width={24} fill={mobileButtonColor('/panduan')} />}
          /> */}
            <OptionButton type="button" style={{ outline: 'none' }} onClick={toggleOptionModal}>
              <OptionButtonIcon>
                <OptionIcon width={24} fill={isDarkMode ? '#f1f2f3' : '#22272c'} />
              </OptionButtonIcon>
              Lainnya
            </OptionButton>
          </MobileNav>
        </NavGrid>
      )}
      <OptionModal isOpen={isOptionModalOpen} onClose={toggleOptionModal} />
    </Root>
  )
}

export default Navigation
