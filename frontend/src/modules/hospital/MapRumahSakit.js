import * as L from 'leaflet'
import { Map, Marker, TileLayer } from 'react-leaflet'
import styled from '@emotion/styled'
import { css, keyframes } from 'emotion'

const MapWithStyle = styled(Map)`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
`
const pulseMarker = keyframes`
  0% {
    box-shadow: 0 0 0 0 rgba(216, 35, 42, 0.4);
  }
  70% {
    box-shadow: 0 0 0 15px rgba(216, 35, 42, 0);
  }
  100% {
    box-shadow: 0 0 0 0 rgba(216, 35, 42, 0);
  }
`
const icon = L.divIcon({
  className: css`
    width: 10px;
    height: 10px;
    background: #d8232a;
    border-radius: 10px;
    box-shadow: 0 0 0 rgba(216, 35, 42, 0.4);
    animation: ${pulseMarker} 2s infinite;
  `,
  iconSize: L.point(10, 10, true)
})

const MapRumahSakit = ({ location }) => {
  return (
    <MapWithStyle
      zoom={10}
      center={location}
      zoomControl={false}
      whenReady={e => {
        const bound = L.latLng(...location).toBounds(2000)
        e.target.flyToBounds(bound, {
          paddingBottomRight: [0, 200]
        })
      }}
    >
      <Marker position={location} icon={icon} />{' '}
      <TileLayer url="https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png" />
    </MapWithStyle>
  )
}

export default MapRumahSakit
