import * as React from 'react'

import { UnstyledButton, Paragraph, Box } from 'components/design-system'
// eslint-disable-next-line import/no-extraneous-dependencies
import styled from '@emotion/styled-base'
import ResultImage from '../../../assets/images/result-isolate.svg'

const Button = styled(UnstyledButton)`
  height: 48px;
  padding: 12px;
  margin-right: 8px;
  text-transform: captitalize;
  font-weight: 600;
  font-family: IBM Plex Sans;
  background: #3389fe;
  border-radius: 4px;
`
const SurveyInfo = styled(Paragraph)`
  padding-top: 12px;
  font-size: 24px;
  height: 40vh;
`
const SurveyHome: React.FC<{}> = () => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <ResultImage />
      </Box>
      <SurveyInfo>
        Kondisi kesehatan Anda saat ini memerlukan pemantauan mandiri.
        <br />
        <br />
        Mohon berdiam di rumah selama Anda menunjukkan gejala. Pastikan Anda menjaga jarak dengan
        orang lain, termasuk penghuni rumah yang lainnya.
      </SurveyInfo>
      <Box display="flex" alignItems="center" justifyContent="center">
        <a href="https://kawalcovid19.id/content/960/prosedur-mendirikan-pusat-karantina-di-luar-fasilitas-kesehatan">
          <Button>Cek Panduan Karantina</Button>
        </a>
      </Box>
    </>
  )
}

export default SurveyHome
