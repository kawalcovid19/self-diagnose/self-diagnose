import * as React from 'react'

import { UnstyledButton, Paragraph, Box } from 'components/design-system'
// eslint-disable-next-line import/no-extraneous-dependencies
import styled from '@emotion/styled-base'
import ResultImage from '../../../assets/images/result-healthy.svg'

const Button = styled(UnstyledButton)`
  height: 48px;
  padding: 12px;
  margin-right: 8px;
  text-transform: captitalize;
  font-weight: 600;
  font-family: IBM Plex Sans;
  background: #3389fe;
  border-radius: 4px;
`
const SurveyInfo = styled(Paragraph)`
  padding-top: 12px;
  font-size: 24px;
  height: 40vh;
`
const SurveyHome: React.FC<{}> = () => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <ResultImage />
      </Box>
      <SurveyInfo>
        Tetap tenang, kondisi kesehatan Anda saat ini baik.
        <br />
        <br />
        Selalu jaga kesehatan Anda dan ikuti panduan pemerintah dalam menghadapi COVID-19. Untuk
        panduan lengkap, silakan ketuk tombol di bawah.
      </SurveyInfo>
      <Box display="flex" alignItems="center" justifyContent="center">
        <Button>Cek Panduan Kesehatan</Button>
      </Box>
    </>
  )
}

export default SurveyHome
