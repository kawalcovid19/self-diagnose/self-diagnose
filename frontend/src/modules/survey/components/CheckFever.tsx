import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '3a', label: 'Ya', nextState: 4, answer: 'true' },
  { key: '3b', label: 'Tidak', nextState: 4, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 3,
  question: 'Apakah Anda demam tinggi dengan suhu lebih dari 38° Celsius?'
}))
const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Apakah Anda
          <b> demam tinggi </b>dengan suhu lebih dari 38° Celsius?
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
