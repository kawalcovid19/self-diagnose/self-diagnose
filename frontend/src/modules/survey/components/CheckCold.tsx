import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '5a', label: 'Ya', nextState: 6, answer: 'true' },
  { key: '5b', label: 'Tidak', nextState: 6, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 5,
  question: 'Apakah anda mengalami sakit tenggorokan disertai pilek?'
}))
const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Apakah anda mengalami <b> sakit tenggorokan </b> disertai <b>pilek?</b>
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
