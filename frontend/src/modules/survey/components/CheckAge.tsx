import * as React from 'react'

import { Heading, Paragraph } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'

let configs = [
  { key: '100a', label: 'Pria', nextState: 101, answer: 'true' },
  { key: '100b', label: 'Wanita', nextState: 101, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 100,
  question: ''
}))

const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Heading variant={900} mb="lg" as="h1">
        Silakan masukan umur Anda
      </Heading>
      <Paragraph>Informasi ini ikut menentukan rekomendasi tindakan terbaik untuk Anda.</Paragraph>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
