import * as React from 'react'

import { Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import ButtonGroups from './ButtonGroups'
import SurveyQuestion from './SurveyQuestion'

let configs = [
  { key: '8a', label: 'Ya', nextState: 99, answer: 'true' },
  { key: '8b', label: 'Tidak', nextState: 99, answer: 'false' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 8,
  question: 'Apakah sesak napas yang Anda rasakan menghambat aktivitas sehari-hari?'
}))

const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Apakah sesak napas yang Anda rasakan<b> menghambat aktivitas sehari-hari?</b>
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
