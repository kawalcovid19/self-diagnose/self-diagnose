import * as React from 'react'

import { Paragraph, Box } from 'components/design-system'
import { SurveyPageProps } from 'modules/survey/utils/props'
import SurveyQuestion from './SurveyQuestion'
import ButtonGroups from './ButtonGroups'

let configs = [
  { key: '99a', label: 'Pria', nextState: 101, answer: 'male' },
  { key: '99b', label: 'Wanita', nextState: 101, answer: 'female' }
]
configs = configs.map(config => ({
  ...config,
  questionId: 99,
  question: ''
}))

const SurveyHome: React.FC<SurveyPageProps> = ({ onAnswer }) => {
  return (
    <>
      <Box display="flex" alignItems="center" justifyContent="center">
        <SurveyQuestion>
          Pilih jenis kelamin Anda di bawah ini
          <br />
          <br />
          <Paragraph>
            Informasi ini ikut menentukan rekomendasi tindakan terbaik untuk Anda.
          </Paragraph>
        </SurveyQuestion>
      </Box>
      <ButtonGroups configs={configs} onAnswer={onAnswer} />
    </>
  )
}

export default SurveyHome
