import * as React from 'react'
import { NextPage } from 'next'
import { useRouter } from 'next/router'
import dynamic from 'next/dynamic'
import Head from 'next/head'
import fetch from 'utils/api/fetch'
import styled from '@emotion/styled'
import { Box } from 'components/design-system'
import { PageWrapper } from 'components/layout'
import DetailInformation from 'modules/hospital/DetailInformation'

const MapLoading = styled(Box)`
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  background: #ccc;
`
const MapRumahSakit = dynamic(() => import('modules/hospital/MapRumahSakit'), {
  loading: () => <MapLoading />,
  ssr: false
})

export interface RumahSakitProps {
  id: number
  name: string
  address: string
  phone: string
  lat: number
  long: number
}

const RumahSakitRujukanPage: NextPage<{}> = () => {
  const [rs, setRs] = React.useState<RumahSakitProps>()
  const router = useRouter()
  React.useEffect(() => {
    if (router.query.id) {
      const url = `${process.env.BACKEND_API_BASE}/api/hospitals/get/${router.query.id}`
      fetch(url).then(json => {
        setRs(json[0])
      })
    }
  }, [router.query.id])
  return (
    <PageWrapper isFullScreen={true} title="Rumah Sakit Rujukan">
      <Head>
        <link rel="stylesheet" href="//unpkg.com/leaflet@1.6.0/dist/leaflet.css" />
        <meta name="format-detection" content="telephone=no"></meta>
      </Head>
      {rs && (
        <>
          <MapRumahSakit location={[rs.lat, rs.long]} />
          <DetailInformation name={rs.name} address={rs.address} phone={rs.phone} />
        </>
      )}
    </PageWrapper>
  )
}

export default RumahSakitRujukanPage
