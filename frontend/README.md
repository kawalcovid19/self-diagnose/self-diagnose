# Self Diagnose Frontend

> Source code for Kawal COVID-19 Self Diagnose Frontend

## Getting Started

### Installation

Run the following command to install the required dependencies. Note that we use [Yarn](https://yarnpkg.com/), not npm.

```bash
yarn
```

### Developing

```bash
# serve with hot reload at localhost:3000
yarn dev

# build for production
yarn build

# start production build locally (note you need to run `yarn build` first)
yarn start
```
